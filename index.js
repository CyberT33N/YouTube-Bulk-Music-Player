/*
███████████████████████████████████████████████████████████████████████████████
██******************** PRESENTED BY t33n Software ***************************██
██                                                                           ██
██                  ████████╗██████╗ ██████╗ ███╗   ██╗                      ██
██                  ╚══██╔══╝╚════██╗╚════██╗████╗  ██║                      ██
██                     ██║    █████╔╝ █████╔╝██╔██╗ ██║                      ██
██                     ██║    ╚═══██╗ ╚═══██╗██║╚██╗██║                      ██
██                     ██║   ██████╔╝██████╔╝██║ ╚████║                      ██
██                     ╚═╝   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝                      ██
██                                                                           ██
███████████████████████████████████████████████████████████████████████████████
████████████████████████████████████████████████████████████████████████████████
*/

let YTLinks // []

import log from 'fancy-log'

import { startBROWSER } from './services/bot.js'
import startYT from './src/startYT.js'
import { parseYoutTubeLinks } from './services/lib.js'

// Advertisement
import('./lib/ads.js')

try {
    // get youtube links from bookmarks.txt
    YTLinks = await parseYoutTubeLinks()

    if (!YTLinks) {
        throw new Error('No video was able to get parsed..')
    }

    log('We successfully parsed all YouTube links.. YTLinks[0]: ' + YTLinks[0])

    // start browser and get page & client
    const pptr = await startBROWSER()
    log('startBROWSER() done..')

    if (!pptr) {
        throw new Error('Something went wrong we cant find pptr')
    }

    const client = pptr.client
    const page = pptr.page

    await startYT(YTLinks, client, page)
    log('finish..')
} catch (e) {
    log('ASYNC - MAIN - error :' + e + '\nYTLinks[0]: ', YTLinks[0] )
}



