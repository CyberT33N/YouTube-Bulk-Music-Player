'use strict'

import fs from 'fs-extra'
import log from 'fancy-log'
import unq from 'unq'

async function parseYoutTubeLinks() {
    log( 'parseYoutTubeLinks()' )


    const YTLinks = []
    const data = await fs.readFile('./bookmarks.txt', 'utf-8')
    log( 'Successfully read bookmarks.txt' )

    // https://www.youtube.com/watch?v=
    const tmpAR = data.match(/youtube[.]com[/]watch[?]v[=](.*?)["]|youtube[.]com[/]watch[?]v[=](.*?)[']|youtube[.]com[/]watch[?]v[=](.*?)(\r\n|\r|\n)/gmi)


    if ( tmpAR[0] ) {
        for ( const d of tmpAR ) {
            YTLinks.push( 'https://' + d.replace(/["]$/gmi, '').replace(/[']$/gmi, '').replace(/(\r\n|\r|\n)/gmi, '') )
        }

        // randomise array..
        await shuffle(YTLinks)

        // unique array
        return unq(YTLinks)
    } //   if( tmpAR ){
    else return false
} //   async function parseYoutTubeLinks(){


async function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1

        // And swap it with the current element.
        temporaryValue = array[currentIndex]
        array[currentIndex] = array[randomIndex]
        array[randomIndex] = temporaryValue
    }

    return array
} // async function shuffle(array) {


async function deleteOfflineVideos(YTLinks) {
    log( 'deleteOfflineVideos() - Current video: ' + YTLinks[0] + '\n\n' )


    let data = await fs.readFile('./bookmarks.txt', 'utf-8')
    log( '#2 - Successfully read bookmarks.txt' + '\n\n' )

    // replace offline video..
    data = data.split( YTLinks[0].replace('https://', '').replace('http://', '').replace('https://www.', '').replace('http://www.', '') ).join('\n')


    await fs.writeFile('./bookmarks.txt', data)
    log( 'Successfully deleted offline video.. We go now back to the script..' + '\n\n' )

    YTLinks.shift()
    return YTLinks
} // async function deleteOfflineVideos(){


async function convertTime(duration) {
    let a = duration.match(/\d+/g)

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
        a = [0, a[0], 0]
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
        a = [a[0], 0, a[1]]
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
        a = [a[0], 0, 0]
    }

    duration = 0

    if (a.length == 3) {
        duration = duration + parseInt(a[0]) * 3600
        duration = duration + parseInt(a[1]) * 60
        duration = duration + parseInt(a[2])
    }

    if (a.length == 2) {
        duration = duration + parseInt(a[0]) * 60
        duration = duration + parseInt(a[1])
    }

    if (a.length == 1) {
        duration = duration + parseInt(a[0])
    }

    duration = Number( duration.toString() + '000' )
    return duration
} //     async function convertTime(duration) {

export { parseYoutTubeLinks, convertTime, deleteOfflineVideos }
