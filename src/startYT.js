/*
███████████████████████████████████████████████████████████████████████████████
██******************** PRESENTED BY t33n Software ***************************██
██                                                                           ██
██                  ████████╗██████╗ ██████╗ ███╗   ██╗                      ██
██                  ╚══██╔══╝╚════██╗╚════██╗████╗  ██║                      ██
██                     ██║    █████╔╝ █████╔╝██╔██╗ ██║                      ██
██                     ██║    ╚═══██╗ ╚═══██╗██║╚██╗██║                      ██
██                     ██║   ██████╔╝██████╔╝██║ ╚████║                      ██
██                     ╚═╝   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝                      ██
██                                                                           ██
███████████████████████████████████████████████████████████████████████████████
████████████████████████████████████████████████████████████████████████████████
*/

import log from 'fancy-log'

import {
    openLink, youTubeError, checkSignBox, scrapVideoInfo,
    checkADS, startVideo, checkAcceptCookies, checkGoogleCaptcha,
    clickTheatreMode
} from '../services/bot.js'

const startYT = async(YTLinks, client, page) => {
    let nextVid

    if (!YTLinks[0]) {
        log( `############ FINISH ##############
        No more youtube video was found.. We will end the script now.. Thank you for using this Bot :)\n\n`)

        await client.close()
        setTimeout(() => {
            process.exit()
        }, 5000)

        return
    }
    log( 'startYT() -  Current URL: ' + YTLinks[0] + '\n\n' )

    // open youtube link..
    if ( !await openLink(YTLinks, page) ) {
        return await startYT(YTLinks, client, page)
    }

    // Check for accept all cookies modal box
    await checkAcceptCookies(page)

    // check if any youtube error like private video and so on is visible..
    ;({ YTLinks, nextVid } = await youTubeError(YTLinks, page))

    if ( nextVid ) {
        return await startYT(YTLinks, client, page)
    }

    // check if signin box is avaible..
    await checkSignBox(page)

    // scrap video details like likes, views, ..
    await scrapVideoInfo(page)

    ;({ YTLinks, nextVid } = await checkGoogleCaptcha(page, YTLinks))

    if (nextVid) {
        return await startYT(YTLinks, client, page)
    }

    // check for video ads.. do it 2 times because sometimes 2 ads..
    await checkADS(page, YTLinks)
    await checkADS(page, YTLinks)

    // Click theatre mode
    await clickTheatreMode(page)

    // start video..
    YTLinks = await startVideo(page, YTLinks)

    // go to next video..
    await startYT(YTLinks, client, page)
}

export default startYT
